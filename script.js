// [SECTION] Javascript Synchronous vs Asynconous 
// JS is by default is synchronous meaning that only once statement is executed at a time.

// This can be proven when a statement has an error, JS will not proceed with the next statement
console.log("Hello World");
//conosle.log("Hello Again"); // syntax error
console.log("Goodbye");

/*
for (let i=0; i<=1500; i++){
	console.log(i);
}
// This code will not execute while the loop code above is not yet done
console.log("Hello Again");
*/


// Asyncronous 
	// means that we can proceed to execute other statements, while time consuming code/s is running in the background 


// The Fetch API allows you to asyncronouse request for a resource data
// A "promise" is an object that represents the equal completion(or failure) of an asyncronous function and it's resulting value
// Syntax: fetch('URL')
console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

fetch("https://jsonplaceholder.typicode.com/posts")
// the "then" method capstures the "response" object and returns another promise which will eventually be resolved or rejected
.then(response => console.log(response.status));


fetch("https://jsonplaceholder.typicode.com/posts")

// use the "json" method from the "response" object and returns another "promise"
.then((response)=> response.json())


// print converted JSON value from the "fetch request"
.then((json) => console.log(json))



// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code

async function fetchData(){
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");
	console.log(result);

	let json = await result.json();
	console.log(json)
}

fetchData();


// [SECTION] Getting a specific document 
fetch("https://jsonplaceholder.typicode.com/posts/1") 

	.then((response) => response.json())
	.then((json) => console.log(json))



// Retrieving titles of the whole collection
/* fetch("https://jsonplaceholder.typicode.com/posts") //fetch the whole collection

	.then((response) => response.json()) // it converts json object response to js object
	.then((json) => {json.forEach(post => console.log(post.title))});
*/

// Retrieving a title of a specific document
fetch("https://jsonplaceholder.typicode.com/posts/87") //fetch the whole collection

	.then((response) => response.json()) // it converts json object response to js object
	.then((json) => console.log(`The retrieved title "${json.title}"`));





// [SECTION] Creating a specific post

fetch("https://jsonplaceholder.typicode.com/posts/",{

	method: "POST",
	headers: {
		"Content-type" : "Application/json"
	},
	body: JSON.stringify({
		title: "OOTD",
		body: "New Outfit",
		userId: 1
	})
})					// converts our json response
.then((response)=>response.json())
.then((json)=> console.log(json));



// [SECTION] Updating a post using PUT Method 
fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "PUT",
	headers: {
		"Content-type" : "Application/json"
	},
	body: JSON.stringify({
		id: 1,
		title: "New car",
		body: "Just got my new red Tesla",
		userId: 1,
	})
})	
.then((response)=>response.json())
.then((json)=> console.log(json));
 


// [SECTION] Updating a post using PATCH method 
//  PUT VS PATCH
	// PATCH is used to update a single/several properties
	// PUT is used to update the whole document/object
fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "PATCH",
	headers: {
		"Content-type" : "Application/json"
	},
	body: JSON.stringify({
		title: "Corrected post title",
	})
})	
.then((response)=>response.json())
.then((json)=> console.log(json));
 


fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "DELETE",
	})
.then((response)=>response.json())
.then((json)=> console.log(json));
 











